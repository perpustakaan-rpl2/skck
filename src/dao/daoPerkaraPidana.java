/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

/**
 *
 * @author Raga
 */

import java.sql.Connection;
import koneksi.koneksi;
import models.modelPerkaraPidana;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class daoPerkaraPidana implements implementPerkaraPidana{
    
    Connection con;
    final String insert = "INSERT INTO perkara_pidana (perkara, putusan, kasus, proses_perkara, pelanggaran, prose_pelanggaran) VALUES (?, ?, ?, ?, ?, ?);";
    
    public daoPerkaraPidana() {
        con = koneksi.connection();
    }

    public void insert(modelPerkaraPidana b) {
        PreparedStatement statement = null;
        try {
            statement = con.prepareStatement(insert);
            statement.setString(1, b.getPerkara());
            statement.setString(2, b.getPutusan());
            statement.setString(3, b.getKasus());
            statement.setString(4, b.getProses_perkara());
            statement.setString(5, b.getPelanggaran());
            statement.setString(6, b.getProse_pelanggaran());
            statement.executeUpdate();
            ResultSet rs = statement.getGeneratedKeys();
            while(rs.next()) {
                b.setId(rs.getInt(1));
            }
        } catch(SQLException ex) {
            ex.printStackTrace();
        } finally {
            try {
                statement.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }  
}