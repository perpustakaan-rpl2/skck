/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import koneksi.koneksi;
import models.modelLampiran;

/**
 *
 * @author rafli
 */
public class daoLampiran implements implementLampiran{
    Connection con;
    final String insert = "INSERT INTO lampiran (ktp, paspor, kartu_keluarga, akta_ijazah, sidik_jari) VALUES (?, ?, ?, ?, ?)";
    
    public daoLampiran(){
        con = koneksi.connection();
    }
    
    public void insert(modelLampiran b){
        PreparedStatement st = null;
        
        try{
           st = con.prepareStatement(insert);
           st.setString(1, b.getKtp());
           st.setString(2, b.getPaspor());
           st.setString(3, b.getKartuKeluarga());
           st.setString(4, b.getAktaLahir());
           st.setString(5, b.getSidikJari());
           st.executeUpdate();  
        }catch(SQLException e){
            e.printStackTrace();
        }finally{
            try{
                st.close();
            }catch(SQLException ex){
                ex.printStackTrace();
            }
        }
    }
}
