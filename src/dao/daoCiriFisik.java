/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import javax.swing.JOptionPane;
import koneksi.koneksi;
import models.modelCiriFisik;

/**
 *
 * @author Ibang
 */
public class daoCiriFisik implements implementCiriFisik {
    Connection con;
    final String insert = "INSERT INTO ciri_fisik (rambut, wajah, kulit, tinggi_badan, berat_badan, tanda_istimewa, rumus_jari_kiri, rumus_jari_kanan) VALUES (?, ?, ?, ?, ?, ?, ?, ?);";
    
    public daoCiriFisik() {
        con = koneksi.connection();
    }

    @Override
    public void insert(modelCiriFisik m) {  
        PreparedStatement st = null;
        try {
            st = con.prepareStatement(insert);
            st.setString(1, m.getRambut());
            st.setString(2, m.getWajah());
            st.setString(3, m.getKulit());
            st.setString(4, m.getTinggi());
            st.setString(5, m.getBerat());
            st.setString(6, m.getTanda());
            st.setString(7, m.getJariKiri());
            st.setString(8, m.getJariKanan());
            st.executeUpdate();
        } catch(SQLException ex) {
            ex.printStackTrace();
        } finally {
            try {
                st.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }
}
