/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import koneksi.koneksi;
import models.modelKeterangan;

/**
 *
 * @author Ibang
 */
public class daoKeterangan implements implementKeterangan {
    Connection con;
    final String insert = "INSERT INTO keterangan (riwayat, hobi, alamat_telp, sponsor, alamat_sponsor, telp_fax, jenis_usaha, alamat_email) VALUES (?, ?, ?, ?, ?, ?, ?, ?);";
    
    public daoKeterangan() {
        con = koneksi.connection();
    }

    @Override
    public void insert(modelKeterangan m) {
        PreparedStatement st = null;
        try {
            st = con.prepareStatement(insert);
            st.setString(1, m.getRiwayat());
            st.setString(2, m.getHobi());
            st.setString(3, m.getAlamat());
            st.setString(4, m.getSponsor());
            st.setString(5, m.getAlamatSponsor());
            st.setString(6, m.getTelpSponsor());
            st.setString(7, m.getJenisUsaha());
            st.setString(8, m.getEmail());
            st.executeUpdate();
        } catch(SQLException ex) {
            ex.printStackTrace();
        } finally {
            try {
                st.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }
    
}
