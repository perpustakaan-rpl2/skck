/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import dao.daoLampiran;
import dao.implementLampiran;
import java.io.File;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import koneksi.koneksi;
import models.modelLampiran;
import view.viewHome;

/**
 *
 * @author rafli
 */
public class controllerLampiran {
    Connection con;
    viewHome vHome;
    implementLampiran iLampiran;
    
    public controllerLampiran(viewHome vHome){
        this.vHome = vHome;
        iLampiran = new daoLampiran();
    }
    
    public void createFolder(){
        File theDir = new File("image");
        boolean result = false;
        if(!theDir.exists()){
            try{
            theDir.mkdir();
                result = true;
            }catch(SecurityException e){
                        
            }
        }
    }
    
    public void insert(){
        if(vHome.getfKtp().getText().trim().equals("")){
            JOptionPane.showMessageDialog(null, "Kolom Ktp Harus Diisi", "Peringatan", JOptionPane.WARNING_MESSAGE);
        }else if(vHome.getfPaspor().getText().trim().equals("")){
            JOptionPane.showMessageDialog(null, "Kolom Paspor Harus Diisi", "Peringatan", JOptionPane.WARNING_MESSAGE);
        }else if(vHome.getfKk().getText().trim().equals("")){
            JOptionPane.showMessageDialog(null, "Kolom Kartu Keluarga Harus Diisi", "Peringatan", JOptionPane.WARNING_MESSAGE);
        }else if(vHome.getfAkta().getText().trim().equals("")){
            JOptionPane.showMessageDialog(null, "Kolom Akta Kelahiran / Ijazah Harus Diisi", "Peringatan", JOptionPane.WARNING_MESSAGE);
        }else if(vHome.getfSidikJari().getText().trim().equals("")){
            JOptionPane.showMessageDialog(null, "Kolom Akta Sidik Jari Harus Diisi", "Peringatan", JOptionPane.WARNING_MESSAGE);
        }else{
            try{
                modelLampiran b = new modelLampiran();
                b.setKtp(vHome.getfKtp().getText());
                b.setPaspor(vHome.getfPaspor().getText());
                b.setKartuKeluarga(vHome.getfKk().getText());
                b.setAktaLahir(vHome.getfAkta().getText());
                b.setSidikJari(vHome.getfSidikJari().getText());
        
                iLampiran.insert(b);
                
                vHome.getjTabbedPane1().setSelectedIndex(7);
            }catch(Exception e){
                JOptionPane.showMessageDialog(null, "Error 404", "Gagal", JOptionPane.WARNING_MESSAGE);
            }
        }
    }
}
