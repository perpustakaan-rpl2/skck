/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

/**
 *
 * @author Raga
 */
import dao.daoPerkaraPidana;
import dao.implementPerkaraPidana;
import models.modelPerkaraPidana;
import view.viewHome;
import java.util.List;
import javax.swing.JOptionPane;

public class controllerPerkaraPidana {

    viewHome vhome;
    implementPerkaraPidana iPerkaraPidana;
    List<modelPerkaraPidana> modelPerkaraPidana;

    public controllerPerkaraPidana(viewHome vhome) {
        this.vhome = vhome;
        iPerkaraPidana = new daoPerkaraPidana();

    }

    public void insert() {

        modelPerkaraPidana b = new modelPerkaraPidana();
        b.setPerkara(vhome.getJ1().getText());
        b.setPutusan(vhome.getJ2().getText());
        b.setKasus(vhome.getJ3().getText());
        b.setProses_perkara(vhome.getJ4().getText());
        b.setPelanggaran(vhome.getJ5().getText());
        b.setProse_pelanggaran(vhome.getJ6().getText());

        iPerkaraPidana.insert(b);
        JOptionPane.showMessageDialog(null, " Data Sukses di Simpan");
    }
}
