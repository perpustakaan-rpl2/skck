/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import javax.swing.JOptionPane;
import models.modelCiriFisik;
import models.modelKeterangan;
import view.viewHome;

/**
 *
 * @author Ibang
 */
public class controllerKeterangan {
    viewHome view;
    controllerInsert cInsert = new controllerInsert();
    
    public controllerKeterangan(viewHome view) {
        this.view = view;
    }
    
    public void submitForm() {
        if(view.getfKetEmail().getText().trim().equals("")) {
            JOptionPane.showMessageDialog(view, "Alamat Email tidak boleh kosong!");
        } else {
            cInsert.insert();
        }
    }
    
    public void simpanKeterangan() {
        modelKeterangan m = new modelKeterangan();
        m.setRiwayat(view.getfRiwayat().getText());
        m.setHobi(view.getfHobi().getText());
        m.setAlamat(view.getfAlamatTelp().getText());
        m.setSponsor(view.getfSponsor().getText());
        m.setAlamatSponsor(view.getfAlamatSponsor().getText());
        m.setTelpSponsor(view.getfTelpFax().getText());
        m.setJenisUsaha(view.getfJenisUsaha().getText());
        m.setEmail(view.getfKetEmail().getText());
        m.setSyaratKetentuan(view.getfSyarat().isSelected());
    }
}
