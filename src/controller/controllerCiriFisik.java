/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import dao.daoCiriFisik;
import dao.implementCiriFisik;
import java.util.List;
import javax.swing.JOptionPane;
import models.modelCiriFisik;
import view.viewHome;

/**
 *
 * @author Ibang
 */
public class controllerCiriFisik {
    viewHome view;
//    implementCiriFisik icf;
    controllerInsert cInsert;
    modelCiriFisik m = new modelCiriFisik();
    
    public controllerCiriFisik(viewHome view) {
        this.view = view;
//        icf = new daoCiriFisik();
    }

    
    public void nextCiriFisik() {
        if(view.getfRambut().getText().trim().equals("")) {
            JOptionPane.showMessageDialog(view, "Rambut tidak boleh kosong!");
        } else if(view.getfWajah().getText().trim().equals("")) {
            JOptionPane.showMessageDialog(view, "Wajah tidak boleh kosong!");
        } else if(view.getfKulit().getText().trim().equals("")) {
            JOptionPane.showMessageDialog(view, "Kulit tidak boleh kosong!");
        } else if(view.getfTinggi().getText().trim().equals("")) {
            JOptionPane.showMessageDialog(view, "Tinggi Badan tidak boleh kosong!");
        } else if(view.getfBerat().getText().trim().equals("")) {
            JOptionPane.showMessageDialog(view, "Berat Badan tidak boleh kosong!");
        } else if(view.getfTanda().getText().trim().equals("")) {
            JOptionPane.showMessageDialog(view, "Tanda Istimewa tidak boleh kosong!");
        } else if(view.getfSidikKiri().getText().trim().equals("")) {
            JOptionPane.showMessageDialog(view, "Rumus Sidik Jari Kiri tidak boleh kosong!");
        } else if(view.getfSidikKanan().getText().trim().equals("")) {
            JOptionPane.showMessageDialog(view, "Rumus Sidik Jari Kanan tidak boleh kosong!");
        } else {
            simpanCiriFisik();
            view.getjTabbedPane1().setSelectedIndex(6);
        }
    }
    
    public void simpanCiriFisik() {
        m.setRambut(view.getfRambut().getText());
        m.setWajah(view.getfWajah().getText());
        m.setKulit(view.getfKulit().getText());
        m.setTinggi(view.getfTinggi().getText());
        m.setBerat(view.getfBerat().getText());
        m.setTanda(view.getfTanda().getText());
        m.setJariKiri(view.getfSidikKiri().getText());
        m.setJariKanan(view.getfSidikKanan().getText());
//        icf.insert(m);
    }
}
