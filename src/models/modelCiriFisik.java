/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

/**
 *
 * @author Ibang
 */
public class modelCiriFisik {
    private Integer id;
    private String rambut;
    private String wajah;
    private String kulit;
    private String tinggi;
    private String berat;
    private String tanda;
    private String jariKiri;
    private String jariKanan;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getRambut() {
        return rambut;
    }

    public void setRambut(String rambut) {
        this.rambut = rambut;
    }

    public String getWajah() {
        return wajah;
    }

    public void setWajah(String wajah) {
        this.wajah = wajah;
    }

    public String getKulit() {
        return kulit;
    }

    public void setKulit(String kulit) {
        this.kulit = kulit;
    }

    public String getTinggi() {
        return tinggi;
    }

    public void setTinggi(String tinggi) {
        this.tinggi = tinggi;
    }

    public String getBerat() {
        return berat;
    }

    public void setBerat(String berat) {
        this.berat = berat;
    }

    public String getTanda() {
        return tanda;
    }

    public void setTanda(String tanda) {
        this.tanda = tanda;
    }

    public String getJariKiri() {
        return jariKiri;
    }

    public void setJariKiri(String jariKiri) {
        this.jariKiri = jariKiri;
    }

    public String getJariKanan() {
        return jariKanan;
    }

    public void setJariKanan(String jariKanan) {
        this.jariKanan = jariKanan;
    }
    
    
}
