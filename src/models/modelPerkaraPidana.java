/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

/**
 *
 * @author Raga
 */
public class modelPerkaraPidana {
    private Integer id;
    private String perkara;
    private String putusan;
    private String kasus;
    private String proses_perkara;
    private String pelanggaran;
    private String prose_pelanggaran;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPerkara() {
        return perkara;
    }

    public void setPerkara(String perkara) {
        this.perkara = perkara;
    }

    public String getPutusan() {
        return putusan;
    }

    public void setPutusan(String putusan) {
        this.putusan = putusan;
    }

    public String getKasus() {
        return kasus;
    }

    public void setKasus(String kasus) {
        this.kasus = kasus;
    }

    public String getProses_perkara() {
        return proses_perkara;
    }

    public void setProses_perkara(String proses_perkara) {
        this.proses_perkara = proses_perkara;
    }

    public String getPelanggaran() {
        return pelanggaran;
    }

    public void setPelanggaran(String pelanggaran) {
        this.pelanggaran = pelanggaran;
    }

    public String getProse_pelanggaran() {
        return prose_pelanggaran;
    }

    public void setProse_pelanggaran(String proses_pelanggaran) {
        this.prose_pelanggaran = proses_pelanggaran;
    }
}
