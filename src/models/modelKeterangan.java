/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

/**
 *
 * @author Ibang
 */
public class modelKeterangan {
    private Integer id;
    private String riwayat;
    private String hobi;
    private String alamat;
    private String sponsor;
    private String alamatSponsor;
    private String telpSponsor;
    private String jenisUsaha;
    private String email;
    private Boolean syaratKetentuan;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getRiwayat() {
        return riwayat;
    }

    public void setRiwayat(String riwayat) {
        this.riwayat = riwayat;
    }

    public String getHobi() {
        return hobi;
    }

    public void setHobi(String hobi) {
        this.hobi = hobi;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getSponsor() {
        return sponsor;
    }

    public void setSponsor(String sponsor) {
        this.sponsor = sponsor;
    }

    public String getAlamatSponsor() {
        return alamatSponsor;
    }

    public void setAlamatSponsor(String alamatSponsor) {
        this.alamatSponsor = alamatSponsor;
    }

    public String getTelpSponsor() {
        return telpSponsor;
    }

    public void setTelpSponsor(String telpSponsor) {
        this.telpSponsor = telpSponsor;
    }

    public String getJenisUsaha() {
        return jenisUsaha;
    }

    public void setJenisUsaha(String jenisUsaha) {
        this.jenisUsaha = jenisUsaha;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Boolean getSyaratKetentuan() {
        return syaratKetentuan;
    }

    public void setSyaratKetentuan(Boolean syaratKetentuan) {
        this.syaratKetentuan = syaratKetentuan;
    }
    
    
}
