/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

/**
 *
 * @author rafli
 */
public class modelLampiran {
    private String ktp;
    private String paspor;
    private String kartuKeluarga;
    private String aktaLahir;
    private String sidikJari;

    public String getKtp() {
        return ktp;
    }

    public void setKtp(String ktp) {
        this.ktp = ktp;
    }

    public String getPaspor() {
        return paspor;
    }

    public void setPaspor(String paspor) {
        this.paspor = paspor;
    }

    public String getKartuKeluarga() {
        return kartuKeluarga;
    }

    public void setKartuKeluarga(String kartuKeluarga) {
        this.kartuKeluarga = kartuKeluarga;
    }

    public String getAktaLahir() {
        return aktaLahir;
    }

    public void setAktaLahir(String aktaLahir) {
        this.aktaLahir = aktaLahir;
    }

    public String getSidikJari() {
        return sidikJari;
    }

    public void setSidikJari(String sidikJari) {
        this.sidikJari = sidikJari;
    }
    
}
